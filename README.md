# Fibber 

Fibber is a free application for creating visual novelties or quests 🌟

The application consists of three projects that work in conjunction with each other.

### Admin

Application backend, uses Strapi

### Constructor

The web part of the visual designer. In order to start working with the constructor, you must first enable the Admin project.

### Game

An application using Capicator.js collects data from Admin (Backend) and creates the final application for users.

# Get started

Install dependencies:

```bash
cd admin && yarn
cd constructor && yarn
```

Run:

```bash
cd admin && yarn develop
cd constructor && yarn start
```

Also applications have `ecosystem.config` ready to be used with [pm2](https://pm2.keymetrics.io/):

```bash
pm2 start admin && pm2 start constructor
```

# Feedback 🫶
I am writing the project alone, I would be happy to get any feedback.
`@h3lpkey`