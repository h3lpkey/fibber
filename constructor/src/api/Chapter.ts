import gql from "graphql-tag";

export const FETCH_CHAPTERS = gql`
  query Chapters {
    chapters {
      data {
        id
        attributes {
          Name
          Description
          Image {
            data {
              id
              attributes {
                url
                name
              }
            }
          }
        }
      }
    }
  }
`;

export const FETCH_CHAPTERS_BY_QUEST = gql`
  query Chapters($questId: ID!) {
    chapters(filters: { quest: { id: { eq: $questId } } }) {
      data {
        id
        attributes {
          Name
          Description
          Image {
            data {
              id
              attributes {
                url
                name
              }
            }
          }
        }
      }
    }
  }
`;

export const CREATE_CHAPTER = gql`
  mutation CreateChapter($name: String!, $description: String, $image: ID!, $quest: ID!) {
    createChapter(
      data: { Name: $name, Description: $description, Image: $image, quest: $quest }
    ) {
      data {
        id
        attributes {
          Name
          Description
        }
      }
    }
  }
`;

export const DELETE_CHAPTER = gql`
  mutation deleteChapter($id: ID!) {
    deleteChapter(id: $id) {
      data {
        id
      }
    }
  }
`;

export const UPDATE_CHAPTER = gql`
  mutation updateChapter(
    $id: ID!
    $name: String!
    $description: String
    $image: ID!
  ) {
    updateChapter(
      id: $id
      data: { Name: $name, Description: $description, Image: $image }
    ) {
      data {
        id
      }
    }
  }
`;
