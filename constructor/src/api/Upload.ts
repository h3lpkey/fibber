import gql from "graphql-tag";

export const QUERY_UPLOAD = gql`
  query Uploads {
    uploadFiles {
      data {
        id
        attributes {
          name
          url
          ext
        }
      }
    }
  }
`;

export const QUERY_UPLOAD_IMAGES = gql`
  query Uploads {
    uploadFiles {
      data {
        id
        attributes {
          url
          name
          ext
        }
      }
    }
  }
`;

// export const QUERY_UPLOAD_IMAGES = gql`
//   query Uploads {
//     uploadFiles(filters: { ext: { eq: ".png" } }) {
//       data {
//         id
//         attributes {
//           url
//           name
//           ext
//         }
//       }
//     }
//   }
// `;

export const UPLOAD = gql`
  mutation ($file: Upload!) {
    upload(file: $file) {
      data {
        id
        attributes {
          name
          url
          ext
        }
      }
    }
  }
`;

export const DELETE_UPLOAD = gql`
  mutation ($id: ID!) {
    deleteUploadFile(id: $id) {
      data {
        id
      }
    }
  }
`;
