import gql from "graphql-tag";

export const FETCH_QUESTS = gql`
  query Quests {
    quests {
      data {
        id
        attributes {
          Name
          Description
          Image {
            data {
              id
              attributes {
                url
                name
              }
            }
          }
        }
      }
    }
  }
`;

export const CREATE_QUEST = gql`
  mutation CreateQuest($name: String!, $description: String, $image: ID!) {
    createQuest(
      data: { Name: $name, Description: $description, Image: $image }
    ) {
      data {
        id
        attributes {
          Name
          Description
        }
      }
    }
  }
`;

export const DELETE_QUEST = gql`
  mutation deleteQuest($id: ID!) {
    deleteQuest(id: $id) {
      data {
        id
      }
    }
  }
`;

export const UPDATE_QUEST = gql`
  mutation updateQuest(
    $id: ID!
    $name: String
    $description: String
    $image: ID
  ) {
    updateQuest(
      id: $id
      data: { Name: $name, Description: $description, Image: $image }
    ) {
      data {
        id
      }
    }
  }
`;
