import gql from "graphql-tag";

export const FETCH_SCENES_BY_CHAPER = gql`
  query Scenes($chapterId: ID!) {
    scenes(filters: { chapter: { id: { eq: $chapterId } } }) {
      data {
        id
        attributes {
          Text
          Person {
            Name
            Image {
              data {
                attributes {
                  url
                }
              }
            }
            AlignLeft
          }
          Background {
            data {
              attributes {
                url
              }
            }
          }
          Music {
            data {
              attributes {
                url
              }
            }
          }
          Link {
            id
            Text
            scene {
              data {
                id
              }
            }
            TriggerSetter
            TriggerGetter
            TriggerDelete
          }
          Map {
            X
            Y
            Type
          }
        }
      }
    }
  }
`;

export const FETCH_SCENE_BY_ID = gql`
  query Scenes($sceneId: ID!) {
    scene(id: $sceneId) {
      data {
        id
        attributes {
          Text
          Person {
            id
            Name
            Image {
              data {
                id
                attributes {
                  url
                }
              }
            }
            AlignLeft
          }
          Background {
            data {
              attributes {
                url
              }
            }
          }
          Music {
            data {
              attributes {
                url
              }
            }
          }
          Link {
            Text
            scene {
              data {
                id
              }
            }
            TriggerSetter
            TriggerGetter
            TriggerDelete
          }
        }
      }
    }
  }
`;

export const FETCH_ALL_GETTERS = gql`
  query Scenes {
    scene {
      data {
        attributes {
          Link {
            TriggerSetter
            TriggerGetter
            TriggerDelete
          }
        }
      }
    }
  }
`;

export const UPDATE_SCENE = gql`
  mutation updateScene($id: ID!, $data: SceneInput!) {
    updateScene(id: $id, data: $data) {
      data {
        id
      }
    }
  }
`;

export const CREATE_SCENE = gql`
  mutation createScene($data: SceneInput!) {
    createScene(data: $data) {
      data {
        id
      }
    }
  }
`;
export const DELETE_SCENE = gql`
  mutation deleteScene($id: ID!) {
    deleteScene(id: $id) {
      data {
        id
      }
    }
  }
`;
