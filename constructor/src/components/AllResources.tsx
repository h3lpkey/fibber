import { DeleteOutlined, InboxOutlined } from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Card, Image, message } from "antd";
import Meta from "antd/lib/card/Meta";
import Dragger from "antd/lib/upload/Dragger";
import { DELETE_UPLOAD, QUERY_UPLOAD, UPLOAD } from "api/Upload";
import { ReactElement } from "react";
import { useAuth } from "utils/auth";
import { urlToData } from "utils/urls";
import "./AllResources.sass";

const AllResources = ({
  callback,
}: {
  callback?: (file: {
    attributes: { name: string; url: string };
    id: string;
  }) => void;
}): ReactElement => {
  const auth = useAuth();
  const { data, loading, error, refetch } = useQuery(QUERY_UPLOAD);
  const [deleteUpload] = useMutation(DELETE_UPLOAD);
  const [addFile] = useMutation(UPLOAD);

  if (loading) return <p>Loading...</p>;
  if (error) {
    if (error.message.includes("Received status code 401")) {
      auth.signout(() => {
        message.warning(`Please, login`);
      });
    } else {
      message.error(`Error: ${JSON.stringify(error)}`);
    }
  }

  return (
    <div className="all-resources">
      <Dragger
        name="file"
        multiple={true}
        showUploadList={false}
        customRequest={(info) => {
          addFile({ variables: { file: info.file } }).then(() => {
            refetch();
          });
        }}
      >
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">
          Click or drag file to this area to upload
        </p>
        <p className="ant-upload-hint">
          Support for a single or bulk upload. Strictly prohibit from uploading
          company data or other band files
        </p>
      </Dragger>
      <div className="resources-gallery">
        {data.uploadFiles.data.map(
          (
            file: { attributes: { name: string; url: string }; id: string },
            index: number
          ) => {
            return (
              <Card
                key={index}
                className="resources-item"
                cover={
                  <Image
                    src={`${urlToData}${file.attributes.url}`}
                  />
                }
                onClick={() => {
                  if (callback) callback(file);
                }}
                actions={[
                  <DeleteOutlined
                    key="ellipsis"
                    onClick={() => {
                      deleteUpload({ variables: { id: file.id } }).then(() => {
                        refetch();
                      });
                    }}
                  />,
                ]}
              >
                <Meta
                  className="resources-item-meta"
                  title={file.attributes.name}
                  description="This is the description"
                />
              </Card>
            );
          }
        )}
      </div>
    </div>
  );
};

export default AllResources;
