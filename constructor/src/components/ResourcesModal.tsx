import { Modal } from "antd";
import { ReactElement, useEffect, useState } from "react";
import AllResources from "./AllResources";

const ResourcesModal = ({
  isModalVisible,
  closesModal,
  selectedResources,
}: {
  isModalVisible: boolean;
  closesModal: () => void;
  selectedResources: (file: {
    attributes: { name: string; url: string };
    id: string;
  }) => void;
}): ReactElement => {
  const [localView, setLocalView] = useState(isModalVisible);

  useEffect(() => {
    setLocalView(isModalVisible);
  }, [isModalVisible]);

  return (
    <Modal
      title="Select image"
      className="modal-resources"
      closable
      open={localView}
      footer
      onCancel={() => {
        setLocalView(false);
        closesModal();
      }}
      width="800"
    >
      <AllResources callback={selectedResources} />
    </Modal>
  );
};

export default ResourcesModal;
