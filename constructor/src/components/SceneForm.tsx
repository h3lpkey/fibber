import {
  DeleteOutlined,
  FileImageOutlined,
  PlayCircleOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import {
  Button,
  Collapse,
  Drawer,
  Form,
  Image,
  Input,
  message,
  Select,
  Space,
  Switch,
} from "antd";
import { CREATE_SCENE, FETCH_SCENE_BY_ID, UPDATE_SCENE } from "api/Scene";
import { ReactElement, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { selectSceneId, selectTriggers, setSceneId } from "store/UISlice";
import { QLTScene } from "types/Scene";
import { urlToData } from "utils/urls";
import ResourcesModal from "./ResourcesModal";
import "./SceneForm.sass";

const { TextArea } = Input;
const { Option } = Select;
const { Panel } = Collapse;

const SceneForm = ({
  visible,
  setVisible,
  lastMousePosition,
}: {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  lastMousePosition: number[];
}): ReactElement => {
  const dispatch = useDispatch();
  const { id: chapterId } = useParams();
  const [form] = Form.useForm();
  const sceneId = useSelector(selectSceneId);
  const [updateScene] = useMutation(UPDATE_SCENE);
  const [createScene] = useMutation(CREATE_SCENE);
  const [resourcesModalVisible, setResourcesModalVisible] = useState(false);
  const [resourceModalFor, setResourceModalFor] = useState<
    "PersonImage" | "Background" | "Music"
  >("PersonImage");

  const {
    data: SceneData,
    loading,
    error,
    refetch,
  } = useQuery(FETCH_SCENE_BY_ID, {
    variables: { sceneId: sceneId },
    skip: sceneId === null,
  });
  const triggers = useSelector(selectTriggers);

  const setResourcesFromModal = (file: {
    attributes: { name: string; url: string };
    id: string;
  }) => {
    setResourcesModalVisible(false);
    const { Person: PersonFromForm } = form.getFieldsValue();
    const data =
      resourceModalFor === "PersonImage"
        ? {
            Person: {
              id: PersonFromForm.id,
              Name: PersonFromForm.Name,
              Image: file.id,
              AlignLeft: PersonFromForm.AlignLeft,
            },
          }
        : { [resourceModalFor]: file.id };
    updateScene({
      variables: {
        id: sceneId,
        data: data,
      },
    }).then(() => {
      refetch();
    });
  };

  useEffect(() => form.resetFields(), [SceneData]); // save data

  const Person = Form.useWatch("Person", form);
  const Background = Form.useWatch("Background", form);
  const Music = Form.useWatch("Music", form);

  const getInitValues = () => {
    let initData: any = {
      Person: {
        Name: "",
        AlignLeft: false,
        Image: null,
        id: null,
      },
    };
    if (SceneData && SceneData.scene) {
      const {
        scene: {
          data: {
            attributes: { Background, Person, Text, Link, Music },
          },
        },
      }: QLTScene = SceneData;
      initData = {
        Person: {
          Name: Person ? Person.Name : "",
          AlignLeft: Person ? Person.AlignLeft : false,
          Image: Person ? Person.Image.data?.id : null,
          ImageUrl: Person ? Person.Image.data?.attributes.url : null,
          id: Person ? Person.id : null,
        },
        // TODO?: refactor? that looks scary
        Links: Link
          ? Link.map((Link) => {
              return {
                Text: Link.Text,
                TriggerSetter: Link.TriggerSetter,
                TriggerGetter: Link.TriggerGetter?.split(","),
                TriggerDelete: Link.TriggerDelete?.split(","),
                scene:
                  typeof Link.scene === "object" && Link.scene?.data
                    ? Link.scene?.data.id
                    : null,
              };
            })
          : [],
        Text: Text,
        Background,
        Music,
      };
    }
    return initData;
  };

  const saveScene = () => {
    // Form save
    const { Person: PersonFromForm, Text, Links } = form.getFieldsValue();
    let LinksRefactor;
    if (Links) {
      LinksRefactor = Links.map((Link: any) => {
        // TODO?: refactor? that looks scary
        return {
          Text: Link.Text,
          TriggerSetter: Link.TriggerSetter && Link.TriggerSetter,
          TriggerGetter: Link.TriggerGetter && Link.TriggerGetter.join(","),
          TriggerDelete: Link.TriggerDelete && Link.TriggerDelete.join(","),
          scene: Link.scene,
        };
      });
    }
    let dataToSave: any = {
      variables: {
        data: {
          Person: {
            Name: PersonFromForm.Name,
            Image: PersonFromForm.Image, // diff object
            AlignLeft: PersonFromForm.AlignLeft,
          },
          Link: LinksRefactor,
          Text: Text,
        },
      },
    };
    console.log("save", dataToSave);
    console.log("sceneId", sceneId);
    if (sceneId) {
      // update scene
      dataToSave.variables.id = sceneId;
      updateScene(dataToSave)
        .then(() => {
          message.success("Saved");
          refetch();
        })
        .catch((error) => {
          message.error(error);
        });
    } else {
      // create scene
      dataToSave.variables.data = {
        chapter: chapterId,
        Map: {
          X: lastMousePosition[0],
          Y: lastMousePosition[1],
          Type: "Start",
        },
      };
      createScene(dataToSave)
        .then(() => {
          message.success("Saved");
          refetch();
        })
        .catch((error) => {
          message.error(error);
        });
    }
  };

  const closeDrawerAndForm = () => {
    dispatch(setSceneId(null));
    form.setFieldsValue({});
    setVisible(false);
  };

  return (
    <>
      <Drawer
        title="Edit scene"
        placement="right"
        onClose={closeDrawerAndForm}
        open={visible}
        footer={
          <Space>
            <Button type="primary" onClick={saveScene}>
              Save
            </Button>
          </Space>
        }
      >
        <Form
          initialValues={getInitValues()}
          layout="vertical"
          className="scene-form"
          form={form}
        >
          <Form.Item hidden name={["Person", "id"]}>
            <Input />
          </Form.Item>
          <Form.Item hidden name={["Person", "ImageUrl"]}>
            <Input />
          </Form.Item>
          <Form.Item label="Person Name" name={["Person", "Name"]}>
            <Input />
          </Form.Item>
          <Form.Item label="Person Image" name={["Person", "Image"]}>
            {Person && Person.Image ? (
              <div className="form-image-wrapper">
                <Image
                  width={200}
                  className="form-image"
                  alt={Person.ImageUrl}
                  src={`${urlToData}${Person.ImageUrl}`}
                />
                <div className="form-image-buttons">
                  <Button
                    onClick={() => {
                      setResourceModalFor("PersonImage");
                      setResourcesModalVisible(true);
                    }}
                    icon={<FileImageOutlined />}
                  >
                    Resources
                  </Button>
                  <Button
                    onClick={() => {
                      const { Person: PersonFromForm } = form.getFieldsValue();
                      updateScene({
                        variables: {
                          id: sceneId,
                          data: {
                            Person: {
                              id: PersonFromForm.id,
                              Name: PersonFromForm.Name,
                              Image: null,
                              AlignLeft: PersonFromForm.AlignLeft,
                            },
                          },
                        },
                      }).then(() => {
                        refetch();
                      });
                    }}
                    danger
                    icon={<DeleteOutlined />}
                  >
                    Clear
                  </Button>
                </div>
              </div>
            ) : (
              <>
                <Button
                  onClick={() => {
                    setResourceModalFor("PersonImage");
                    setResourcesModalVisible(true);
                  }}
                >
                  Resources
                </Button>
              </>
            )}
          </Form.Item>
          <Form.Item
            label="Person Align"
            name={["Person", "AlignLeft"]}
            valuePropName="checked"
          >
            <Switch />
          </Form.Item>
          <Form.Item label="Text" name="Text">
            <TextArea />
          </Form.Item>
          <Form.Item label="Music" name="Music">
            {Music && Music.data?.attributes.url ? (
              <div className="form-image-wrapper">
                <Button icon={<PlayCircleOutlined />}>
                  {Music.data?.attributes.url}
                </Button>
                <div className="form-image-buttons">
                  <Button
                    onClick={() => {
                      setResourceModalFor("Music");
                      setResourcesModalVisible(true);
                    }}
                    icon={<FileImageOutlined />}
                  >
                    Resources
                  </Button>
                  <Button
                    onClick={() => {
                      updateScene({
                        variables: {
                          id: sceneId,
                          data: {
                            Music: null,
                          },
                        },
                      }).then(() => {
                        refetch();
                      });
                    }}
                    danger
                    icon={<DeleteOutlined />}
                  >
                    Clear
                  </Button>
                </div>
              </div>
            ) : (
              <>
                <Button
                  onClick={() => {
                    setResourceModalFor("Music");
                    setResourcesModalVisible(true);
                  }}
                >
                  Resources
                </Button>
              </>
            )}
          </Form.Item>
          <Form.Item label="Background" name="Background">
            {Background && Background.data ? (
              <div className="form-image-wrapper">
                <Image
                  width={200}
                  className="form-image"
                  src={`${urlToData}${Background.data?.attributes.url}`}
                />
                <div className="form-image-buttons">
                  <Button
                    onClick={() => {
                      setResourceModalFor("Background");
                      setResourcesModalVisible(true);
                    }}
                    icon={<FileImageOutlined />}
                  >
                    Resources
                  </Button>
                  <Button
                    onClick={() => {
                      updateScene({
                        variables: {
                          id: sceneId,
                          data: {
                            Background: null,
                          },
                        },
                      }).then(() => {
                        refetch();
                      });
                    }}
                    danger
                    icon={<DeleteOutlined />}
                  >
                    Clear
                  </Button>
                </div>
              </div>
            ) : (
              <>
                <Button
                  onClick={() => {
                    setResourceModalFor("Background");
                    setResourcesModalVisible(true);
                  }}
                >
                  Resources
                </Button>
              </>
            )}
          </Form.Item>
          <Form.Item label="Buttons">
            <Form.List name="Links">
              {(fields, { add, remove }) => (
                <>
                  {fields.map(({ key, name, ...restField }) => (
                    <div
                      id={key.toString()}
                      key={key}
                      className="links-block-item"
                    >
                      <div className="form-link">
                        <div className="form-link-inputs">
                          <Form.Item
                            {...restField}
                            name={[name, "Text"]}
                            label="Text"
                          >
                            <TextArea placeholder="Text" />
                          </Form.Item>
                          <Collapse>
                            <Panel header="Triggers" key={"1"}>
                              <Form.Item
                                {...restField}
                                name={[name, "TriggerSetter"]}
                                label="Setter"
                              >
                                <Input />
                              </Form.Item>
                              <Form.Item
                                {...restField}
                                name={[name, "TriggerGetter"]}
                                label="Getter"
                              >
                                <Select mode="multiple" allowClear>
                                  {triggers.map((trigger) => {
                                    return (
                                      <Option value={trigger} key={trigger}>
                                        {trigger}
                                      </Option>
                                    );
                                  })}
                                </Select>
                              </Form.Item>
                              <Form.Item
                                {...restField}
                                name={[name, "TriggerDelete"]}
                                label="Delete"
                              >
                                <Select mode="multiple" allowClear>
                                  {triggers.map((trigger) => {
                                    return (
                                      <Option value={trigger} key={trigger}>
                                        {trigger}
                                      </Option>
                                    );
                                  })}
                                </Select>
                              </Form.Item>
                            </Panel>
                          </Collapse>
                        </div>
                        <DeleteOutlined onClick={() => remove(name)} />
                      </div>
                    </div>
                  ))}
                  <Form.Item>
                    <Button
                      type="dashed"
                      onClick={() => add()}
                      block
                      icon={<PlusOutlined />}
                    >
                      Add link
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Form.Item>
        </Form>
      </Drawer>
      <ResourcesModal
        isModalVisible={resourcesModalVisible}
        closesModal={() => {
          setResourcesModalVisible(false);
        }}
        selectedResources={setResourcesFromModal}
      />
    </>
  );
};

export default SceneForm;
