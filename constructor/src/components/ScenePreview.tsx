import { Drawer } from "antd";
import { ReactElement } from "react";
import Scene from "./Scene";

const ScenePreview = ({
  visible,
  setVisible,
}: {
  visible: boolean;
  setVisible: (visible: boolean) => void;
}): ReactElement => {
  return (
    <Drawer
      title="Preview scene"
      placement="right"
      onClose={() => setVisible(false)}
      open={visible}
    >
      <Scene />
    </Drawer>
  );
};

export default ScenePreview;
