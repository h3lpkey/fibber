import { DeleteOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/react-hooks";
import { Button, List, message, Tag, Tooltip, Typography } from "antd";
import { DELETE_SCENE } from "api/Scene";
import { ALPHABET } from "constants/common";
import { Handle, Position } from "react-flow-renderer";
import { TScene } from "types/Scene";
import { urlToData } from "utils/urls";
import "./NodeScene.sass";

const { Text } = Typography;

const NodeScene = ({
  data,
}: {
  data: TScene & {
    openEditor: (id: number) => void;
    openPreview: (id: number) => void;
    refetchDataCallback: () => void;
  };
}) => {
  const [deleteScene] = useMutation(DELETE_SCENE);
  return (
    <div className="node-scene">
      <Handle
        type="target"
        className="handler handler-top"
        position={Position.Top}
      />
      <div className="node-scene-body">
        <div className="node-scene-body-buttons">
          <div className="node-scene-body-buttons-group">
            <Tooltip title="Edit">
              <Button
                type="primary"
                shape="circle"
                icon={<EditOutlined />}
                size="small"
                onClick={() => {
                  data.openEditor(data.id);
                }}
              />
            </Tooltip>
            <Tooltip title="Preview">
              <Button
                type="primary"
                shape="circle"
                icon={<EyeOutlined color="red" />}
                size="small"
                onClick={() => {
                  data.openPreview(data.id);
                }}
              />
            </Tooltip>
          </div>
          <Tooltip title="Delete">
            <Button
              type="primary"
              shape="circle"
              icon={<DeleteOutlined />}
              size="small"
              onClick={() => {
                deleteScene({ variables: { id: data.id } })
                  .then(() => {
                    message.success("Deleted");
                    data.refetchDataCallback();
                  })
                  .catch((error) => {
                    message.error(error);
                  });
              }}
            />
          </Tooltip>
        </div>
        <div className="node-scene-body-images">
          <div className="node-scene-body-background">
            {data.attributes.Background && (
              <img
                className="node-scene-body-background-image"
                src={`${urlToData}${data.attributes.Background.data?.attributes.url}`}
                alt="background"
              />
            )}
          </div>
          {data.attributes.Person && (
            <div
              className={`node-scene-body-person ${
                data.attributes.Person.AlignLeft ? "left" : "right"
              }`}
            >
              <img
                className="node-scene-body-person-image"
                src={`${urlToData}${data.attributes.Person.Image.data?.attributes.url}`}
                alt="person"
              />
            </div>
          )}
        </div>
        {data.attributes.Person && (
          <div className="node-scene-body-text">
            Name: {data.attributes.Person.Name}
          </div>
        )}
        <div className="node-scene-body-text">Text: {data.attributes.Text}</div>
        <div className="node-scene-body-links">
          {data.attributes.Link && (
            <List itemLayout="vertical" size="small" header="Buttons:">
              {data.attributes.Link.map((Link, index) => {
                return (
                  <List.Item className="node-scene-body-links-link" key={index}>
                    <Text italic>- "{Link.Text}"</Text>
                    <br />
                    {Link.TriggerSetter?.split(",").map(
                      (setter: string, index) => {
                        return (
                          <Tag key={index} color="blue">
                            {setter}
                          </Tag>
                        );
                      }
                    )}
                    <br />
                    {Link.TriggerGetter?.split(",").map(
                      (getter: string, index) => {
                        return (
                          <Tag key={index} color="green">
                            {getter}
                          </Tag>
                        );
                      }
                    )}
                    <br />
                    {Link.TriggerDelete?.split(",").map(
                      (del: string, index) => {
                        return (
                          <Tag key={index} color="volcano">
                            {del}
                          </Tag>
                        );
                      }
                    )}
                    <Handle
                      type="source"
                      position={Position.Right}
                      id={ALPHABET[index]}
                      className="handler handler-button"
                    />
                  </List.Item>
                );
              })}
            </List>
          )}
        </div>
      </div>
    </div>
  );
};

export default NodeScene;
