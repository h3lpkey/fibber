import { BorderOuterOutlined } from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { message } from "antd";
import { FETCH_SCENES_BY_CHAPER, UPDATE_SCENE } from "api/Scene";
import { ALPHABET } from "constants/common";
import { useCallback, useEffect, useRef, useState } from "react";
import ReactFlow, {
  addEdge,
  applyEdgeChanges,
  applyNodeChanges,
  Background,
  BackgroundVariant,
  Connection,
  ConnectionLineType,
  ControlButton,
  Controls,
  Edge,
  EdgeChange,
  Node,
  NodeChange,
  XYPosition,
} from "react-flow-renderer";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { setSceneId, setTriggers } from "store/UISlice";
import { TLink, TScene } from "types/Scene";
import { useAuth } from "utils/auth";
import "./Map.sass";
import NodeScene from "./NodeScene";
import SceneForm from "./SceneForm";
import ScenePreview from "./ScenePreview";

const nodeTypes = {
  NodeScene: NodeScene,
};

const Map = () => {
  // TODO?: refactor? Maybe 2-3 components? or logic to another file?
  const auth = useAuth();
  const dispatch = useDispatch();
  const { id: chapterId } = useParams();
  const reactFlowWrapper = useRef<HTMLDivElement>(null);

  const { data, loading, error, refetch } = useQuery(FETCH_SCENES_BY_CHAPER, {
    variables: { chapterId: chapterId },
  });
  const [updateScene] = useMutation(UPDATE_SCENE);
  const [nodes, setNodes] = useState<Node[]>([]);
  const [edges, setEdges] = useState<Edge[]>([]);
  const [mousePosition, setMousePosition] = useState<number[]>([0, 0]);
  const [rfInstance, setRfInstance] = useState<any>(null);

  const [visibleEditorDrawer, setVisibleEditorDrawer] = useState(false);
  const [visiblePreviewDrawer, setVisiblePreviewDrawer] = useState(false);

  const onSceneConnect = (connection: Connection) => {
    // update
    const LinkIndex = ALPHABET.findIndex(
      (value) => value === connection.sourceHandle
    );
    const nodeShouldUpdate = nodes.find(
      (node) => node.id === connection.source
    );

    const newLinksRaw: TLink[] = [...nodeShouldUpdate?.data.attributes.Link];
    const newLinkRaw = Object.assign({}, newLinksRaw[LinkIndex]);
    newLinkRaw.scene = connection.target;
    newLinksRaw[LinkIndex] = newLinkRaw;

    const newLinks = newLinksRaw.map((link, index) => {
      const tempLink = { ...link };
      // @ts-ignore
      delete tempLink.__typename;
      if (typeof tempLink.scene === "object") {
        // @ts-ignore
        delete tempLink.scene;
      }
      return tempLink;
    });

    updateScene({
      variables: {
        id: connection.source,
        data: {
          Link: newLinks,
        },
      },
    });
  };

  const onSave = (id: string, position: XYPosition) => {
    if (rfInstance) {
      const flow = rfInstance.toObject();
      flow.nodes.map((node: any) => {
        updateScene({
          variables: {
            id: id,
            data: {
              Map: {
                Y: parseInt(position.y.toFixed()),
                X: parseInt(position.x.toFixed()),
              },
            },
          },
        });
      });
    }
  };

  // if new data - create nodes and edges
  useEffect(() => {
    const triggers: string[] = [];
    const newEdges: Edge[] = [];
    if (data) {
      const scenes = data.scenes.data;
      const newNodes = scenes.map((sceneData: TScene) => {
        const scene = { ...sceneData } as TScene & {
          openEditor: (id: string) => void;
          openPreview: (id: string) => void;
          refetchDataCallback: () => void;
        };
        scene.attributes.Link?.forEach((link, index) => {
          if (
            link.id &&
            scene.id &&
            link.scene &&
            typeof link.scene === "object" &&
            link.scene.data
          ) {
            const edge = {
              id: `${link.id}-${scene.id}`,
              source: `${scene.id}`,
              target: `${link.scene.data.id}`,
              sourceHandle: ALPHABET[index],
            };
            newEdges.push(edge);
          }
          if (link.TriggerSetter) triggers.push(link.TriggerSetter);
        });
        scene.openEditor = (id: string) => {
          setVisibleEditorDrawer(true);
        };
        scene.openPreview = (id: string) => {
          setVisiblePreviewDrawer(true);
        };
        scene.refetchDataCallback = () => {
          refetch();
        };
        return {
          id: `${scene.id}`,
          type: "NodeScene",
          data: scene,
          position: {
            x: scene.attributes.Map.X,
            y: scene.attributes.Map.Y,
          },
        };
      });

      setNodes(newNodes);
      setEdges(newEdges);
      dispatch(setTriggers(triggers));
    }
  }, [data]);

  const onNodesChange = useCallback(
    (changes: NodeChange[]) => {
      setNodes((nds) => {
        return applyNodeChanges(changes, nds);
      });
    },
    [setNodes]
  );
  const onEdgesChange = useCallback(
    (changes: EdgeChange[]) => {
      setEdges((eds) => {
        return applyEdgeChanges(changes, eds);
      });
    },
    [setEdges]
  );
  const onConnect = useCallback(
    (connection: Connection) => {
      onSceneConnect(connection);
      setEdges((eds) => {
        return addEdge(connection, eds);
      });
    },
    [setEdges, nodes]
  );

  if (loading) return <p>Loading...</p>;

  if (error) {
    if (error.message.includes("Received status code 401")) {
      auth.signout(() => {
        message.warning(`Please, login`);
      });
    } else {
      message.error(`Error: ${JSON.stringify(error)}`);
    }
  }

  return (
    <div className="map-wrapper" ref={reactFlowWrapper}>
      <ReactFlow
        className="constructor-map"
        fitView
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        connectionLineType={ConnectionLineType.Step}
        onConnect={onConnect}
        nodeTypes={nodeTypes}
        onInit={setRfInstance}
        onNodeClick={(e, node) => {
          dispatch(setSceneId(node.id));
        }}
        onNodeDragStop={(event, { id, position }) => {
          onSave(id, position);
        }}
        onConnectStop={(event) => {
          if (event && event.target) {
            const element = event.target as Element;
            const targetIsPane = element.classList.contains("react-flow__pane");
            if (targetIsPane && reactFlowWrapper.current) {
              const { clientX, clientY } = event;
              const { top, left } =
                reactFlowWrapper.current.getBoundingClientRect();
              setMousePosition([clientX - left - 75, clientY - top]);
              setVisibleEditorDrawer(true);
            }
          }
        }}
      >
        <Background variant={BackgroundVariant.Dots} gap={12} size={0.5} />
      </ReactFlow>
      <SceneForm
        visible={visibleEditorDrawer}
        setVisible={setVisibleEditorDrawer}
        lastMousePosition={mousePosition}
      />
      <ScenePreview
        visible={visiblePreviewDrawer}
        setVisible={setVisiblePreviewDrawer}
      />
    </div>
  );
};

export default Map;
