import { BrowserRouter, Route, Routes } from "react-router-dom";
import LayoutConstructor from "../layouts/Constrcutor";
import PageConstructor from "../pages/PageConstructor";
import PageLogin from "../pages/Login";
import PageChapters from "../pages/PageChapters";
import PageQuests from "../pages/PageQuests";
import PageResources from "../pages/PageResources";
import { RequireAuth, AuthProvider } from "../utils/auth";

export const Router = () => {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Routes>
          <Route
            element={
              <RequireAuth>
                <LayoutConstructor />
              </RequireAuth>
            }
            path="/"
          >
            <Route path="/quests" element={<PageQuests />} />
            <Route path="/constructor/:id" element={<PageConstructor />} />
            <Route path="/chapters/:id" element={<PageChapters />} />
            <Route path="/resources" element={<PageResources />} />
          </Route>
          <Route path="/login" element={<PageLogin />} />
          <Route path="*" element={<div>404</div>} />
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
};

export default Router;
