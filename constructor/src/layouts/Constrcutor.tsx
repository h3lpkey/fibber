import { BookTwoTone, PictureTwoTone } from "@ant-design/icons";
import { Layout, Menu } from "antd";
import fibberLogo from "assets/icons/fibberLogo.png";
import { ReactElement, useEffect, useState } from "react";
import { Outlet, useLocation, useNavigate } from "react-router";
import "./Constructor.sass";

const LayoutConstructor = (): ReactElement => {
  const { Sider, Content } = Layout;
  const navigate = useNavigate();
  const location = useLocation();

  const [collapsed, setCollesped] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);

  const items = [
    {
      label: "Quests",
      key: "/quests",
      icon: <BookTwoTone twoToneColor="#eb2f96" />,
    },
    {
      label: "Resources",
      key: "/resources",
      icon: <PictureTwoTone twoToneColor="#eb2f96" />,
    },
  ];

  useEffect(() => {
    items.forEach((item) => {
      if (location.pathname.includes(item.key)) {
        setSelected([item.key]);
      }
    });
  }, []);

  return (
    <Layout className="layout">
      <Sider
        collapsible
        collapsed={collapsed}
        theme="light"
        onCollapse={setCollesped}
      >
        <div className="logo">
          <img src={fibberLogo} alt="" />
        </div>
        <Menu
          theme="light"
          items={items}
          selectedKeys={selected}
          onClick={(item) => {
            setSelected(item.keyPath);
            navigate(item.key);
          }}
        />
      </Sider>
      <Layout>
        <Content>
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default LayoutConstructor;
