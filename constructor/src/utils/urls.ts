const serverUrl = process.env.REACT_APP_ADMIN_URL;
const serverPort = process.env.REACT_APP_ADMIN_PORT;

export const urlToData = `//${serverUrl}:${serverPort}`;
