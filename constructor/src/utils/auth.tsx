import { message } from "antd";
import axios from "axios";
import { createContext, ReactNode, useContext, useState } from "react";
import { Navigate, useLocation } from "react-router-dom";

interface AuthContextType {
  user: string | null;
  token: string | null;
  signin: (
    values: { identifier: string; password: string },
    callback: VoidFunction
  ) => void;
  signout: (callback: VoidFunction) => void;
  signinFromStorage: (user: string, token: string) => void;
}

const AuthContext = createContext<AuthContextType>(null!);

function useAuth() {
  return useContext(AuthContext);
}

const authFunctions = {
  signin(
    values: { identifier: string; password: string },
    callback: (user: string, token: string) => void
  ) {
    axios
      .post(`${process.env.REACT_APP_ADMIN_URL}/api/auth/local`, {
        ...values,
      })
      .then((response) => {
        callback(response.data.user.username, response.data.jwt);
      })
      .catch((error) => {
        message.error(error.response.data.error.message);
      });
  },
  signout(callback: VoidFunction) {
    setTimeout(callback, 100);
  },
};

const AuthProvider = ({ children }: { children: ReactNode }) => {
  const userStorage = localStorage.getItem("user");
  const tokenStorage = localStorage.getItem("token");

  const [token, setToken] = useState<string | null>(tokenStorage);
  const [user, setUser] = useState<string | null>(userStorage);

  const signin = (
    values: { identifier: string; password: string },
    callback: VoidFunction
  ) => {
    return authFunctions.signin(values, (user: string, token: string) => {
      setToken(token);
      setUser(user);
      localStorage.setItem("token", token);
      localStorage.setItem("user", user);
      callback();
    });
  };

  const signout = (callback: VoidFunction) => {
    return authFunctions.signout(() => {
      setToken(null);
      setUser(null);
      localStorage.setItem("token", "");
      localStorage.setItem("user", "");
      callback();
    });
  };

  const signinFromStorage = (user: string, token: string) => {
    setUser(user);
    setToken(token);
  };

  const value = { user, token, signin, signout, signinFromStorage };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

const RequireAuth = ({ children }: { children: any }) => {
  const auth = useAuth();
  const location = useLocation();

  if (!auth.token) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }

  return children;
};

export { RequireAuth, AuthProvider, useAuth };
