import { ApolloClient, InMemoryCache } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { ApolloLink } from "@apollo/react-hooks";
import { createUploadLink } from "apollo-upload-client";

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("token");
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const link = `//${process.env.REACT_APP_ADMIN_URL}:${process.env.REACT_APP_ADMIN_PORT}/graphql`;

const uploadLink = createUploadLink({
  uri: link,
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: link,
  link: ApolloLink.from([authLink, uploadLink]),
  connectToDevTools: true,
});

export default client;
