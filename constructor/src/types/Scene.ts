export type TBackground = {
  data: {
    attributes: {
      url: string;
    };
    __typename: string;
  };
  __typename: string;
};

export type TMusic = {
  data: {
    attributes: {
      url: string;
    };
    __typename: string;
  };
  __typename: string;
};

export type TPerson = {
  id: string;
  AlignLeft: boolean;
  Image: {
    data: {
      id: string;
      attributes: {
        url: string;
      };
      __typename: string;
    };
    __typename: string;
  };
  Name: string;
  __typename: string;
};

export type TLink = {
  id: string;
  Text: string;
  TriggerDelete: string | null;
  TriggerGetter: string | null;
  TriggerSetter: string | null;
  scene:
    | string
    | null
    | {
        data: {
          id: string;
        };
      };
  __typename: string;
};
export type TMap = {
  X: number;
  Y: number;
  Type: string;
};

export type TScene = {
  id: number;
  attributes: {
    Text?: string;
    Background?: TBackground;
    Link?: TLink[];
    Music?: TMusic;
    Person?: TPerson;
    Map: TMap;
  };
  __typename: string;
};

export type QLTScene = {
  scene: {
    data: TScene;
    __typename: string;
  };
};
