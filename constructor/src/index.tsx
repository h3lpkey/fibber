import { ApolloProvider } from "@apollo/react-hooks";
import "antd/dist/antd.css";
import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import "./assets/sass/main.sass";
import reportWebVitals from "./reportWebVitals";
import Router from "./router/Router";
import { store } from "./store/store";
import client from "./utils/apolloClient";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <ApolloProvider client={client}>
        <Router />
      </ApolloProvider>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
