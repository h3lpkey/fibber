import AllResources from "components/AllResources";
import { ReactElement } from "react";

const PageResources = (): ReactElement => {
  return (
    <div className="page page-resources">
      <AllResources />
    </div>
  );
};

export default PageResources;
