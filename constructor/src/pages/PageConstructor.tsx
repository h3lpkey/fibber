import { ReactElement } from "react";
import Map from "../components/Map";
import "./PageConstructor.sass";

const PageConstructor = (): ReactElement => {
  return (
    <div className="page page-constructor">
      <Map />
    </div>
  );
};

export default PageConstructor;
