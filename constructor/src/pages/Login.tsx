import { Button, Form, Input, Row } from "antd";
import { ReactElement } from "react";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "utils/auth";
import "./PageLogin.sass";

const PageLogin = (): ReactElement => {
  const auth = useAuth();
  const navigate = useNavigate();
  const location = useLocation();

  if (auth.user && auth.token) {
    return <Navigate to="/quests" state={{ from: location }} replace />;
  }

  const onFinish = (values: any) => {
    auth.signin(values, () => {
      navigate("/quests");
    });
  };

  return (
    <div className="page page-login">
      <div className="login">
        <h1 className="title">Fibber</h1>
        <Row justify="center">
          <Form name="basic" onFinish={onFinish} hideRequiredMark>
            <Form.Item
              label="Identifier"
              name="identifier"
              rules={[
                { required: true, message: "Please input your identifier!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" block>
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Row>
      </div>
    </div>
  );
};

export default PageLogin;
