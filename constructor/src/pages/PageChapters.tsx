import {
  DeleteOutlined,
  FolderOpenOutlined,
  PlusOutlined,
  SettingOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { useMutation, useQuery } from "@apollo/react-hooks";
import {
  Button,
  Card,
  Col,
  Drawer,
  Form,
  Input,
  message,
  Popconfirm,
  Row,
  Space,
  Upload,
} from "antd";
import {
  CREATE_CHAPTER,
  DELETE_CHAPTER,
  FETCH_CHAPTERS_BY_QUEST,
  UPDATE_CHAPTER,
} from "api/Chapter";
import { UPLOAD } from "api/Upload";
import ResourcesModal from "components/ResourcesModal";
import { ReactElement, useState } from "react";
import { useNavigate } from "react-router";
import { useParams } from "react-router-dom";
import { useAuth } from "utils/auth";
import { urlToData } from "utils/urls";
import "./PageChapters.sass";
const { Meta } = Card;

const PageChapters = (): ReactElement => {
  const auth = useAuth();
  const { id: questId } = useParams();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [formVisible, setFormVisible] = useState(false);
  const [resourcesModalVisible, setResourcesModalVisible] = useState(false);
  const { data, loading, error, refetch } = useQuery(FETCH_CHAPTERS_BY_QUEST, {
    variables: { questId: questId },
  });
  const [createChapter] = useMutation(CREATE_CHAPTER);
  const [deleteChapter] = useMutation(DELETE_CHAPTER);
  const [updateChapter] = useMutation(UPDATE_CHAPTER);
  const [addFile] = useMutation(UPLOAD);

  if (loading) return <p>Loading...</p>;
  if (error) {
    if (error.message.includes("Received status code 401")) {
      auth.signout(() => {
        message.warning(`Please, login`);
      });
    } else {
      message.error(`Error: ${JSON.stringify(error)}`);
    }
  }
  const setResourcesFromModal = (file: {
    attributes: { name: string; url: string };
    id: string;
  }) => {
    setResourcesModalVisible(false);
    form.setFieldsValue({
      image: [
        {
          name: file.attributes.name,
          id: file.id,
        },
      ],
    });
  };

  return (
    <div className="page page-chapters">
      {/* Main View */}
      <Card
        hoverable
        className="chapter-card chapter-card-new"
        onClick={() => {
          setFormVisible(true);
          form.resetFields();
        }}
      >
        <PlusOutlined />
      </Card>
      {data.chapters.data.map(
        (
          chapter: {
            id: string;
            attributes: {
              Name: string;
              Description: string;
              Image: {
                data?: {
                  id: string;
                  attributes: { url: string; name: string };
                };
              };
            };
          },
          index: number
        ) => {
          return (
            <Card
              hoverable
              className="chapter-card"
              cover={
                <img
                  alt="chapter background"
                  src={`${urlToData}${chapter.attributes.Image.data?.attributes.url}`}
                />
              }
              key={index}
              actions={[
                <SettingOutlined
                  key="setting"
                  onClick={(e) => {
                    e.preventDefault();
                    form.setFieldsValue({
                      id: chapter.id,
                      name: chapter.attributes.Name,
                      description: chapter.attributes.Description,
                      image: [
                        {
                          name: chapter.attributes.Image.data?.attributes.name,
                          id: chapter.attributes.Image.data?.id,
                        },
                      ],
                    });
                    setFormVisible(true);
                  }}
                />,
                <FolderOpenOutlined
                  onClick={() => {
                    navigate(`/constructor/${chapter.id}`);
                  }}
                />,
                <Popconfirm
                  title="Are you sure to delete this chapter?"
                  onConfirm={() => {
                    deleteChapter({ variables: { id: chapter.id } }).then(
                      () => {
                        refetch();
                      }
                    );
                  }}
                  okText="Yes"
                  cancelText="No"
                >
                  <DeleteOutlined />
                </Popconfirm>,
              ]}
            >
              <Meta
                title={`${chapter.attributes.Name}`}
                description={`id: ${chapter.id}`}
              />
            </Card>
          );
        }
      )}
      {/* Chapter Form */}
      <Drawer
        title="Create a new chapter"
        width={720}
        onClose={() => {
          setFormVisible(false);
        }}
        open={formVisible}
        extra={
          <Space>
            <Button
              onClick={() => {
                setFormVisible(false);
              }}
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                const { id, name, description, image } = form.getFieldsValue();
                if (id) {
                  updateChapter({
                    variables: {
                      id,
                      name,
                      description,
                      image: image[0].id,
                    },
                  }).then(() => {
                    refetch();
                    setFormVisible(false);
                  });
                } else {
                  createChapter({
                    variables: {
                      name,
                      description,
                      image: image[0].id,
                    },
                  }).then(() => {
                    refetch();
                    setFormVisible(false);
                  });
                }
              }}
              type="primary"
            >
              Submit
            </Button>
          </Space>
        }
      >
        <Form layout="vertical" form={form}>
          <Form.Item name="id" label="id" hidden>
            <Input />
          </Form.Item>
          <Row>
            <Col span={24}>
              <Form.Item
                name="name"
                label="Name"
                rules={[
                  { required: true, message: "Please enter chapter name" },
                ]}
              >
                <Input placeholder="Please enter chapter name" />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <p>Select from Resources</p>
              <Button
                onClick={() => {
                  setResourcesModalVisible(true);
                }}
              >
                Resources
              </Button>
            </Col>
            <Col span={12}>
              <Form.Item
                name="image"
                label="Image"
                valuePropName="fileList"
                getValueFromEvent={(e) => {
                  if (Array.isArray(e)) {
                    return e;
                  }
                  return e?.fileList;
                }}
              >
                <Upload
                  maxCount={1}
                  customRequest={(info) => {
                    addFile({ variables: { file: info.file } }).then(
                      (response: any) => {
                        form.setFieldsValue({
                          image: [
                            {
                              name: response.data.upload.data.attributes.name,
                              id: response.data.upload.data.id,
                            },
                          ],
                        });
                      }
                    );
                  }}
                >
                  <Button icon={<UploadOutlined />}>Click to Upload</Button>
                </Upload>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item name="description" label="Description">
                <Input.TextArea
                  rows={4}
                  placeholder="please enter description"
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
      <ResourcesModal
        isModalVisible={resourcesModalVisible}
        closesModal={() => {
          setResourcesModalVisible(false);
        }}
        selectedResources={setResourcesFromModal}
      />
    </div>
  );
};

export default PageChapters;
