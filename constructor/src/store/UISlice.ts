import { createSlice } from "@reduxjs/toolkit";

export const ui = createSlice({
  name: "UI",
  initialState: {
    direction: "ltr",
    langue: "English",
    checkoutStep: 1,
    selectSceneId: null,
    triggers: [],
  },
  reducers: {
    setCheckout: (state, action) => {
      state.checkoutStep = action.payload;
    },
    setSceneId: (state, action) => {
      state.selectSceneId = action.payload;
    },
    setTriggers: (state, action) => {
      state.triggers = action.payload;
    },
  },
});

export const { setTriggers, setCheckout, setSceneId } = ui.actions;

export const selectTriggers = (state: { UI: { triggers: string[] } }) =>
  state.UI.triggers;
export const selectDirection = (state: { UI: { direction: "ltr" | "rtl" } }) =>
  state.UI.direction;
export const selectCheckoutPage = (state: { UI: { checkoutStep: number } }) =>
  state.UI.checkoutStep;
export const selectSceneId = (state: {
  UI: { selectSceneId: string | null };
}) => state.UI.selectSceneId;

export default ui.reducer;
