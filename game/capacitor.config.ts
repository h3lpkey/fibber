import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'fibberGame',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
