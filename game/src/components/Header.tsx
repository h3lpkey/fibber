import React from "react";
import "./Header.scss";

const Header = () => {
  return (
    <header className="header header--escape">
      <div className="container header__wrapper">
        <div className="header__back"> </div>
        <div className="header__settings"> </div>
      </div>
    </header>
  );
};

export default Header;
