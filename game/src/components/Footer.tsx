import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="copyright"></div>
      </div>
    </footer>
  );
};

export default Footer;
