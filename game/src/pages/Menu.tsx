import Scene from "layouts/Scene";
import React from "react";
import { Link } from "react-router-dom";
import "./Menu.sass";

const Menu = () => {
  return (
    <Scene>
      <main className="content-bottom">
        <section className="messages messages--preload  messages--bottom">
          <div className="container">
            <div className="messages__corner-shadow">
              <div className="messages__body">
                <span className="messages__textheading">Орбита смерти</span>
                <div className="messages__descr">
                  <p>
                    Коити рождён в космосе и обречён в одиночестве блуждать на
                    орбите погибшей планеты. Он уверен, что является последним
                    представителем вида homo sapiens, вместе с его смертью
                    закончится история человечества. Но так ли это?
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div className="container btn-wrapper--outer">
          <Link to={"/game"}>
            <button className="btn btn--default">Начать игру</button>
          </Link>
        </div>
      </main>
    </Scene>
  );
};

export default Menu;
