import Footer from "components/Footer";
import Header from "components/Header";
import { ReactElement } from "react";
import "../assets/scss/main.scss";
import "../components/_modules.scss";
import "./Scene.scss";

const Scene = (props: {
  children: React.ReactNode | React.ReactNode[];
}): ReactElement => {
  return (
    <div className="scene">
      <Header />
      {props.children}
      <Footer />
    </div>
  );
};

export default Scene;
