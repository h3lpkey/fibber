import { FC, ReactElement } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Menu from "../pages/Menu";
import Game from "../pages/Game";

const Router: FC = (): ReactElement => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={`/`} element={<Menu />} />
        <Route path={`/game`} element={<Game />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
