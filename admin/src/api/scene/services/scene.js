'use strict';

/**
 * scene service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::scene.scene');
