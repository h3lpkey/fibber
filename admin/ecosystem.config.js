module.exports = {
  apps: [{
    script: 'yarn develop',
    watch: '.',
    name: 'admin',
  }],
  env: {
    "PORT": process.env.PORT,
  },
};
